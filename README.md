# Gitlab Timescraper #
 
Sind sie es Leid herauszufinden wann und wie viel Zeit sie in Jira oder sonst wo eintragen müssen? Nun hier ist die perfekte Lösung für sie wenn Gitlab als VCS eingesetzt wird.

## Anforderungen ##

* Gitlab API in Version 4 oder höher
* Python 2.7

## Funktion ##

Mit den Aufruf von **extract.py** wird das angegebene VCS abgefragt und die Zeiten aus den einzelnen Commits für jeden Tag extrahiert. Danach werden diese je nach gewähltem Formatierer in die Standardausgabe geschrieben.

### Dinge welche beachtet werden müssen ###

* Der Name des Repositories welches angegeben wird muss vorhanden sein und darf keine Id, also kein numerischer Wert sein.
* Wenn sie einen Commit am Morgen eines Tages und dann am Abend des Tages gemacht haben, zwischendrin aber etwas anderes nimmt das Tool an das sie **durchgehend** am Projekt gearbeitet haben und die Zeiten werden nicht stimmt

## Installation ##

* Klonen dieses Repositories
* Nach der Installation muss config.json im Unterverzeichnis config eingerichted werden

### Einrichtung config.json ###

In der Konfigurationsdatei **config.json** lassen sich die grundlegenen Umgebungsvariablen für das Tool definieren. Das sind im Moment 2 Stück. Einmal der access-token für die Gitlab API welchen sie in ihrer Gitlab Instanz unter profile/personal_access_tokens erstellen können und der Benutzername nachdem gefiltert werden soll. Im Unterverzeichnis config finden sie bereits eine Datei **config.json.example** welche sie als Vorlage für die Einrichtung benutzen können.

#### Beispielkonfiguration ####

Eine fertig eingrichtete config.json-Datei könnte so aussehen.

```
{
    "access-token": "asdök0135lkfs",
    "username": "alexandro"
}
```

## Benutzung ##

Für die Benutzung des Tool werden mindestens die Parameter **--host** und **--repo** vorausgesetzt. **--host** zeigt dabei auf die Url oder die IP ihrer Gitlab-Instanz. **--repo** ist der Name ihres Repositories. Achtung: Hier werden Benutzname und Projektzugehörigkeit beachtet. 

### Beispiele ###

Für die Beispiele werden wir annehmen das unserer Gitlab-Instanz unter der Url **intrant.gitlab.lan** erreichbar ist. Der Benutzer welchen wir verwenden werden heißt **alexandro** und das Repository welches wird benennen müssen **banane-webapp**.

Das Repository liegt dabei im eigenen Bereich des Nutzers alexadro würde also unter der Url http://intrant.gitlab.lan/alexandro/banane-webapp erreichbar sein.

#### Grundlegene Funktion ####

Ein Aufruf welcher die Minimalanforderung an das Plugin erfüllt sieht wie folgt aus.

`> ./extract.py --host intranet.gitlab.lan --repo alexandro/banane-webapp` 

Dieser Aufruf gibt alle Informationen als json wieder zurück. Für die Filterung nach Zeit ist dies natürlich nicht besonders hilfreicht.

#### Filterung für Jira ####

Zur Zeit kann nur eine Filterung für Jira durchgeführt werden. Dazu übergeben wir einen weiteren Parameter **--format** mit dem Wert **jira**.

`> ./extract.py --host intranet.gitlab.lan --repo alexandro/banane-webapp --format jira` 

#### Gitlab Instanz ist nur über https verfügbar ####

Ist ihre Gitlab Instanz nur über https erreichbar übergeben wir den Parameter **--protocol** mit dem Wert **https**.

`> ./extract.py --host intranet.gitlab.lan --repo alexandro/banane-webapp --protocol https` 

#### Gitlab Instanz läuft auf einem Custom Port ####

Wenn ihre Gitlab Instanz nicht auf einem Standardport (80 für http,443 für https) läuft können sie den ebenfalls als Parameter **--port** übergeben.

`> ./extract.py --host intranet.gitlab.lan --repo alexandro/banane-webapp --port 8080` 

##### Alles zusammen ####

`> ./extract.py --host intranet.gitlab.lan --repo alexandro/banane-webapp --format jira --protocol https --port 8443` 