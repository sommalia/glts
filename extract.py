#!/usr/bin/python

import sys
from models.config import Reader
from models.api import HostInformation, RepoInformation, Consumer
from models.formatting import FormatterFactory


def run():
    required_parameters = ["--host", "--repo"]
    optional_parameters = ["--port", "--protocol", "--branch", "--format"]
    passed_arguments = sys.argv[1:]

    values = {}

    last_parameter = None
    for i,arg in enumerate(passed_arguments):
        if i % 2 == 0:
            # value is key
            if arg in required_parameters or arg in optional_parameters:
                #valid parameter
                last_parameter = arg
            else:
                print "Invalid Parameter passed"
                exit()
        else:
            values[last_parameter] = arg

  
    #check if values are correct
    for req in required_parameters:
        if not req in values.keys():
            print "Required argument {0} is missing".format(req)
            exit()

    for opt in optional_parameters:
        if not opt in values.keys():
            #argument was not set, but is optional so we set it to none
            values[opt] = None       

    r = Reader()
    config = r.get_config();

    host = HostInformation(values['--host'], values['--protocol'], values['--port'])
    repo = RepoInformation(values['--repo'], values['--branch'])

    consumer = Consumer(host, repo, config.token)
    response_content = consumer.get_commits()

    formatter = FormatterFactory().create(values['--format'], response_content, host, repo, config.username)
    formatter.format()

if __name__ == "__main__":
    run()