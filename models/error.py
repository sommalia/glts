class HostInformationException(Exception):
    pass

class RepoInformationException(Exception):
    pass

class ConfigException(Exception):
    pass
