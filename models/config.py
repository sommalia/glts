import json
from os.path import dirname, join, exists
from error import ConfigException

class Reader:
    def __init__(self):
        self.data = {}
        self.config_path = join(dirname(dirname(__file__)), "config/config.json")

        if not exists(self.config_path):
            raise ConfigException("config.json files does not exists under {0}".format(self.config_path))
        else:
            with open(self.config_path, "r") as file:
                self.data = json.load(file)

            #check config file
            if "username" in self.data.keys() and "access-token" in self.data.keys():
                #config file valid
                pass
            else:
                raise ConfigException("config.json is missing values")

    def get_config(self):
        return ConfigInformation(self.data['username'], self.data['access-token'])

class ConfigInformation:
    def __init__(self, username, token):
        self.username = username;
        self.token = token;


        