import urllib, urllib2
import error


class Consumer:
    def __init__(self, host, repo, token):
        self.repo = repo
        self.host = host
        self.token = token;

    def get_commits(self):
        url = self.repo.build_repo_api_link(self.host)
        req = urllib2.Request(url)
        req.add_header("PRIVATE-TOKEN", self.token)
        content = urllib2.urlopen(req).read()
        return content

class RepoInformation:
    def __init__(self, name, branch = None):
  
        if name == "":
            raise error.RepoInformationException("No name or id for the repository given")
        
        self.name_unencoded = name
        self.name = urllib.quote(name, safe='')    #can be the url encoded name of the repository or the project id
        self.branch = branch

    def build_repo_api_link(self, host):
        base = host.build_base_uri();
        return "{0}/api/v4/projects/{1}/repository/commits".format(base, self.name)

    def build_repo_base_link(self, host):
        try:
            value = int(self.name)
        except ValueError:
            #not an it we can contine
            base = host.build_base_uri()
            return "{0}/{1}".format(base, self.name_unencoded)

        raise RepoInformation("Cannot build Repostiory base link with id")
        
     

class HostInformation:
    def __init__(self, host, protocol = None, port = None):
        #check protocol
        if protocol != None and protocol != "http" and protocol != "https":
            raise error.HostInformationException("Invalid Protocol")

        if protocol == None:
            self.protocol = "http"
        else:
            self.protocol = protocol

        self.host = host
        self.port = port

    def build_base_uri(self):
        if self.port == None:
            return "{0}://{1}".format(self.protocol, self.host)
        else:
            return "{0}://{1}:{2}".format(self.protocol, self.host, self.port)
