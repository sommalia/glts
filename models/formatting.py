import json

from datetime import datetime,date
import time

from api import RepoInformation

class FormatterFactory:
    def __init__(self):
        pass
    
    def create(self, type_name, content, host, repo,  username = None):
        if type_name == "jira":
            return JiraFormatter(content, host, repo, username)
        elif type_name == "markdown":
            return MarkdownFormatter(content, host, repo, username)
        else:
            return JsonFormatter(content, host, repo, username)

class JiraFormatter():
    def __init__(self, content, host, repo, username):
        self.content = content
        self.data = json.loads(content)
        self.username = username
        self.repo = repo
        self.host = host

    def format(self):
        entries = []

        for entry in self.data:
            username = entry['committer_name']

            if username == self.username:
                commit_hash = entry["id"]
                commit_link = ""
                title = entry['title']
                date = datetime.strptime(entry['created_at'][0:19], "%Y-%m-%dT%H:%M:%S")
                entries.append(JiraFormatterEntry(title, commit_hash, date))

        #format the entries to display them in jira tickets
        entries.sort(key = lambda x: x.date)

        if len(entries) > 0:
            current_entry = entries[0]
            entries_for_same_day = [current_entry, ]

            counter = 1;

            while counter < len(entries):
                #check if current entry has the same date as the looped over entry
                current_entry_date = current_entry.date.year, current_entry.date.month, current_entry.date.day
                looped_over_entry_date = entries[counter].date.year, entries[counter].date.month, entries[counter].date.day

                if looped_over_entry_date == current_entry_date:
                    # it is the same push to the stack
                    entries_for_same_day.append(entries[counter])

                else:
                    #not the same clear the stack and the current item new
                    self.__print_entries_for_same_day(entries_for_same_day)

                    current_entry = entries[counter]
                    entries_for_same_day = [current_entry]

                counter += 1
            
            self.__print_entries_for_same_day(entries_for_same_day)
           #not the same clear the stack and the current item new
        else:
            #no entries for me in this repository
            pass

        

    def __print_entries_for_same_day(self, entries_for_same_day):
        export_date_format = "%Y.%m.%d %H:%M:%S"
        time_taken_for_commits = 15

        if len(entries_for_same_day) > 1:
            first = time.mktime(entries_for_same_day[0].date.timetuple())
            last = time.mktime(entries_for_same_day[-1].date.timetuple())

            time_taken_for_commits = int((last - first) / 60)
            #round up to the next 15 minuted
            if time_taken_for_commits % 15 > 0:
                time_taken_for_commits = ((time_taken_for_commits / 15) + 1) * 15

        print "Time: {0}m".format(time_taken_for_commits)

        if len(entries_for_same_day) > 1:
      
            first = entries_for_same_day[0].date
            last = entries_for_same_day[-1].date
            print "Start: {0}, Ende: {1}".format(first.strftime(export_date_format), last.strftime(export_date_format))
        else:
            print "Start: {0}".format(entries_for_same_day[0].date.strftime(export_date_format))


        print "Commits: "
        for entry in entries_for_same_day:
            print "* [{0}|{1}]".format(entry.title, entry.create_link_from_hash(self.host, self.repo))
        
        print ""

class JiraFormatterEntry:
    def __init__(self, title, commit_hash, date):
        self.title = title
        self.hash = commit_hash
        self.date = date

    def __str__(self):
        return "title: {0}, date: {1}, hash: {2}".format(self.title, self.date, self.hash)

    def create_link_from_hash(self, host, repo):
        return "{0}/commit/{1}".format(repo.build_repo_base_link(host), self.hash)


class MarkdownFormatter():
    def __init__(self, content, host, repo, username):
        self.content = content

    def format(self):
        return self.content

class JsonFormatter():
    def __init__(self, content, host, repo, username):
        self.content = content

    def format(self):
        return self.content
        